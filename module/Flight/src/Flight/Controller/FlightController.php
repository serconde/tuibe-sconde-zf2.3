<?php

namespace Flight\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
use Zend\Http\Request;
use Zend\Config\Config;
use Zend\Form\Form;
use Zend\Json\Json;
use Zend\Session\Container;
use Zend\I18n\View\Helper\DateFormat;
use Flight\Form\SearchForm;

class FlightController extends AbstractActionController {

    private $apiUri;
    private $apiUsername;
    private $apiPassword;

    public function __construct() {
        $config = new Config(include realpath(__DIR__ . '/../../../config/api.config.php'));

        if (empty($config) || empty($config->uri)) {
            throw new \Exception('API uri not specified');
        } else {
            $this->apiUri = $config->uri;
        }

        $this->apiUsername = isset($config->username) ? $config->username : null;
        $this->apiPassword = isset($config->password) ? $config->password : null;
    }

    /**
     * Index action. Loads the search flight form
     * 
     * @return array
     * @throws \Exception
     */
    public function indexAction() {
        $requestParams = array('locale' => 'en_GB');

        if ($this->params()->fromPost('from')) {
            $requestParams['departureairport'] = $this->params()->fromPost('from');
        } elseif ($this->params()->fromPost('to')) {
            $requestParams['destinationairport'] = $this->params()->fromPost('to');
        }

        $response = $this->__doRequest('flightroutes', $requestParams);

        if (!$response->isOk()) {
            throw new \Exception('Flight routes couldn\'t be retrieved: ' . $response->getStatusCode() . ' - ' . $response->getReasonPhrase());
        } else {
            $jsonResults = $response->getBody();
            $results = Json::decode($jsonResults);
            $flightRoutes = $results->flightroutes;

            $flightFrom = array();
            $flightTo = array();

            foreach ($flightRoutes as $route) {

                if ($this->params()->fromPost('from')) {
                    $flightTo[$route->RetCode] = $route->RetName;
                } elseif ($this->params()->fromPost('to')) {
                    $flightFrom[$route->DepCode] = $route->DepName;
                } else {
                    if (!array_key_exists($route->DepCode, $flightFrom)) {
                        $flightFrom[$route->DepCode] = $route->DepName;
                    }

                    if (!array_key_exists($route->RetCode, $flightTo)) {
                        $flightTo[$route->RetCode] = $route->RetName;
                    }
                }
            }

            //Ajax call: returns the airports connected to a given one
            if ($this->params()->fromPost('ajax')) {
                if ($this->params()->fromPost('from')) {
                    die(Json::encode($flightTo));
                } elseif ($this->params()->fromPost('to')) {
                    die(Json::encode($flightFrom));
                } else {
                    die(json_encode(array($flightFrom, $flightTo)));
                }
            }

            //The search form
            $form = new SearchForm(array(
                'flight_from' => array_merge(array('' => 'From'), $flightFrom),
                'flight_to' => array_merge(array('' => 'To'), $flightTo),
                    )
            );

            //Adding the JS needed by the search form
            $renderer = $this->serviceLocator->get('Zend\View\Renderer\RendererInterface');
            
            $vhm = $this->getServiceLocator()->get('ViewHelperManager');
            $headLink = $vhm->get('headLink');            
            $headLink->prependStylesheet($renderer->basePath('/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.css'))
                    ->prependStylesheet($renderer->basePath('/plugins/jquery-ui-1.12.1.custom/jquery-ui.structure.min.css'))
                    ->prependStylesheet($renderer->basePath('/plugins/jquery-ui-1.12.1.custom/jquery-ui.theme.min.css'))
            ;

            $inlineScript = $vhm->get('inlineScript');

            $inlineScript->appendFile($renderer->basePath('/plugins/jquery-ui-1.12.1.custom/jquery-ui.min.js'))
                    ->appendFile($renderer->basePath('/plugins/jquery-validation-1.15.0/jquery.validate.min.js'))
                    ->appendFile($renderer->basePath('/js/searchForm.js'));
            ;

            return array('form' => $form);
        }
    }

    /**
     * Get the available dates for a route. Ajax call
     * 
     * @throws \Exception
     */
    public function scheduleAction() {
        $requestParams = array('locale' => 'en_GB');
        $requestParams['departureairport'] = $this->params()->fromPost('from');
        $requestParams['destinationairport'] = $this->params()->fromPost('to');

        if ($this->params()->fromPost('flightType') === 'roundtrip') {
            $requestParams['returndepartureairport'] = $this->params()->fromPost('to');
            $requestParams['returndestinationairport'] = $this->params()->fromPost('from');
        }

        $response = $this->__doRequest('flightschedules', $requestParams);

        if (!$response->isOk()) {
            throw new \Exception('Flight schedules couldn\'t be retrieved: ' . $response->getStatusCode() . ' - ' . $response->getReasonPhrase());
        } else {
            $jsonResults = $response->getBody();
            $results = Json::decode($jsonResults);
            $schedules = $results->flightschedules;

            $outbound = $schedules->OUT;
            $departureDates = array();
            $returnDates = array();
            $today = new \DateTime();

            foreach ($outbound as $out) {
                //skipping dates that doesn't have the requested departure and destination airports
                if ($out->DepCode === $requestParams['departureairport'] && $out->RetCode === $requestParams['destinationairport']) {
                    if (!in_array($out->date, $departureDates)) {
                        $date = \DateTime::createFromFormat('Y-m-d', $out->date);

                        if ($today->getTimestamp() <= $date->getTimestamp()) {
                            $departureDates[] = $out->date;
                        }
                    }
                }
            }

            if ($this->params()->fromPost('flightType') === 'roundtrip') {
                $return = $schedules->RET;

                foreach ($return as $ret) {
                    //skipping dates that doesn't have the requested departure and destination airports
                    if ($ret->DepCode === $requestParams['returndepartureairport'] && $ret->RetCode === $requestParams['returndestinationairport']) {
                        if (!in_array($ret->date, $returnDates)) {
                            $date = \DateTime::createFromFormat('Y-m-d', $ret->date);

                            if ($today->getTimestamp() <= $date->getTimestamp()) {
                                $returnDates[] = $ret->date;
                            }
                        }
                    }
                }
            }

            die(Json::encode(array($departureDates, $returnDates)));
        }
    }

    /**
     * Validates that the glight dates are correct
     * Ajax call
     */
    public function checkDatesAction() {
        $depDateStr = $this->params()->fromPost('depDate');
        $retDateStr = $this->params()->fromPost('retDate');
        $today = time();
        $depDate = \DateTime::createFromFormat('d-m-Y', $depDateStr);
        $retDate = \DateTime::createFromFormat('d-m-Y', $retDateStr);
        $result = new \stdClass();
        $result->ok = true;

        if ($today > $depDate->getTimestamp() || $today > $retDate->getTimestamp()) {
            $result->ok = false;
        } elseif ($depDate->getTimestamp() > $retDate->getTimestamp()) {
            $result->ok = false;
        }

        die(Json::encode($result));
    }

    /**
     * Look for the flights that matchers the search options
     * 
     * @return array
     * @throws \Exception
     */
    public function searchAction() {
        if ($this->params()->fromPost('flight_from') == null || $this->params()->fromPost('flight_to') == null || $this->params()->fromPost('flight_departure') == null) {
            return $this->redirect()->toRoute('home');
        }

        $requestParams = array('locale' => 'en_GB');
        $requestParams['departureairport'] = $this->params()->fromPost('flight_from');
        $requestParams['destinationairport'] = $this->params()->fromPost('flight_to');
        $depDate = \DateTime::createFromFormat('d-m-Y', $this->params()->fromPost('flight_departure'))->format('Ymd');
        $requestParams['departuredate'] = $depDate;

//        if ($this->params()->fromPost('flight_type') === 'roundtrip') {
//            $requestParams['returndepartureairport'] = $this->params()->fromPost('flight_to');
//            $requestParams['returndestinationairport'] = $this->params()->fromPost('flight_from');
//            $retDate = \DateTime::createFromFormat('d-m-Y', $this->params()->fromPost('flight_return'))->format('Ymd');
//            $requestParams['returndate'] = $retDate;
//        }

        if ($this->params()->fromPost('flight_pax_adults')) {
            $requestParams['adults'] = $this->params()->fromPost('flight_pax_adults');
        }

        if ($this->params()->fromPost('flight_pax_children')) {
            $requestParams['children'] = $this->params()->fromPost('flight_pax_children');
        }

        if ($this->params()->fromPost('flight_pax_babies')) {
            $requestParams['infants'] = $this->params()->fromPost('flight_pax_babies');
        }

        $response = $this->__doRequest('flightavailability', $requestParams);

        if (!$response->isOk()) {
            throw new \Exception('Flight information couldn\'t be retrieved: ' . $response->getStatusCode() . ' - ' . $response->getReasonPhrase());
        } else {
            $jsonResults = $response->getBody();
            $results = Json::decode($jsonResults);
            $flights = $results->flights;
            $departures = array();
            $returns = array();

            foreach ($flights->OUT as $f) {
                $flight = (object) array();
                $depDateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $f->datetime);
                $flight->date = $depDateTime->format('D d/m/Y');
                $flight->depTime = $depDateTime->format('H:i');
                $flight->arrTime = '--:--';
                $flight->price = $f->price;
                $flight->origin = str_replace(' Airport', '', $f->depart->airport->name);
                $flight->destination = str_replace(' Airport', '', $f->arrival->airport->name);
                $flight->observations = array();

                if ($f->depart->airport->code != $requestParams['departureairport']) {
                    $flight->observations[] = "Departure from $flight->origin";
                }

                if ($f->arrival->airport->code != $requestParams['destinationairport']) {
                    $flight->observations[] = "Arrival in $flight->destination";
                }

                $departures[] = $flight;
            }

            if ($this->params()->fromPost('flight_type') === 'roundtrip') {
//                foreach ($flights->RET as $f) {
//                    $flight = (object) array();
//                    $depDateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $f->datetime);
//                    $flight->date = $depDateTime->format('D d/m/Y');
//                    $flight->depTime = $depDateTime->format('H:i');
//                    $flight->arrTime = '--:--';
//                    $flight->price = $f->price;
//                    $flight->origin = str_replace(' Airport', '', $f->depart->airport->name);
//                    $flight->destination = str_replace(' Airport', '', $f->arrival->airport->name);
//                    $flight->observations = array();
//
//                    if ($f->depart->airport->code != $requestParams['departureairport']) {
//                        $flight->observations[] = "Departure from $flight->origin";
//                    }
//
//                    if ($f->arrival->airport->code != $requestParams['destinationairport']) {
//                        $flight->observations[] = "Arrival in $flight->destination";
//                    }
//
//                    $returns[] = $flight;
//                }

                $requestParams['departureairport'] = $this->params()->fromPost('flight_to');
                $requestParams['destinationairport'] = $this->params()->fromPost('flight_from');
                $retDate = \DateTime::createFromFormat('d-m-Y', $this->params()->fromPost('flight_return'))->format('Ymd');
                $requestParams['departuredate'] = $retDate;

                $response = $this->__doRequest('flightavailability', $requestParams);

                if (!$response->isOk()) {
                    throw new \Exception('Flight information couldn\'t be retrieved: ' . $response->getStatusCode() . ' - ' . $response->getReasonPhrase());
                } else {
                    $jsonResults = $response->getBody();
                    $results = Json::decode($jsonResults);
                    $flights = $results->flights;

                    foreach ($flights->OUT as $f) {
                        $flight = (object) array();
                        $depDateTime = \DateTime::createFromFormat('Y-m-d\TH:i:s', $f->datetime);
                        $flight->date = $depDateTime->format('D d/m/Y');
                        $flight->depTime = $depDateTime->format('H:i');
                        $flight->arrTime = '--:--';
                        $flight->price = $f->price;
                        $flight->origin = str_replace(' Airport', '', $f->depart->airport->name);
                        $flight->destination = str_replace(' Airport', '', $f->arrival->airport->name);
                        $flight->observations = array();

                        if ($f->depart->airport->code != $requestParams['departureairport']) {
                            $flight->observations[] = "Departure from $flight->origin";
                        }

                        if ($f->arrival->airport->code != $requestParams['destinationairport']) {
                            $flight->observations[] = "Arrival in $flight->destination";
                        }

                        $returns[] = $flight;
                    }
                }
            }
        }

        $sessionContainer = new Container('flights');
        $sessionContainer->params = $requestParams;
        $sessionContainer->departures = $departures;
        $sessionContainer->returns = $returns;

        return array('departures' => $departures, 'returns' => $returns);
    }

    /**
     * Get flight details
     * 
     * @return array
     */
    public function detailsAction() {
        $sessionContainer = new Container('flights');
        $outbound = $this->params()->fromPost('flight-outbound');

        if (!is_numeric($outbound)) {
            return $this->redirect()->toRoute('home');
        }

        $ret = $this->params()->fromPost('flight-return');
        $searchParams = $sessionContainer->params;
        $departures = $sessionContainer->departures;
        $returns = $sessionContainer->returns;
        $details = array();

        if (!empty($departures) && isset($departures[$outbound])) {
            $details['outbound'] = $departures[$outbound];
        }

        if (!empty($returns) && isset($returns[$ret])) {
            $details['return'] = $returns[$ret];
        }

        $details['passengers'] = array();

        if (!empty($searchParams['adults'])) {
            $details['passengers'][] = $searchParams['adults'] . ' Adult' . ($searchParams['adults'] == 1 ? '' : 's');
        }

        if (!empty($searchParams['children'])) {
            $details['passengers'][] = $searchParams['children'] . ' Child' . ($searchParams['children'] == 1 ? '' : 'ren');
        }

        if (!empty($searchParams['infants'])) {
            $details['passengers'][] = $searchParams['infants'] . ' Bab' . ($searchParams['infants'] == 1 ? 'y' : 'ies');
        }

        return $details;
    }

    /**
     * Do the request to the webservice
     * 
     * @param string $action
     * @param array $params
     * @param string $method
     * @return mixed
     */
    private function __doRequest($action, $params = array(), $method = 'GET') {
        $client = new Client();
        $client->setUri($this->apiUri . $action);

        if (!empty($this->apiUsername && !empty($this->apiPassword))) {
            $client->encodeAuthHeader($this->apiUsername, $this->apiPassword);
        }

        $adapter = new Curl();
        $adapter->setOptions(array(
            'curloptions' => array(
                CURLOPT_TIMEOUT => 30,
                CURLOPT_RETURNTRANSFER => 1
            )
        ));

        $client->setAdapter($adapter);
        $client->setMethod($method);
        $client->setParameterGet($params);
        $client->send();
        $response = $client->getResponse();
        return $response;
    }

}
