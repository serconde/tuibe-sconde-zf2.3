<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Flight\Controller\Flight' => 'Flight\Controller\FlightController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'Flight\Controller\Flight',
                        'action' => 'index',
                    ),
                ),
            ),
            'schedule' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/schedule',
                    'defaults' => array(
                        'controller' => 'Flight\Controller\Flight',
                        'action' => 'schedule',
                    ),
                ),
            ),  
            'checkdates' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/checkdates',
                    'defaults' => array(
                        'controller' => 'Flight\Controller\Flight',
                        'action' => 'checkDates',
                    ),
                ),
            ),             
            'search' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/search',
                    'defaults' => array(
                        'controller' => 'Flight\Controller\Flight',
                        'action' => 'search',
                    ),
                ),
            ),
            'details' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/details',
                    'defaults' => array(
                        'controller' => 'Flight\Controller\Flight',
                        'action' => 'details',
                    ),
                ),
            ),            
        ),
    ),   
    'view_manager' => array(
        'template_path_stack' => array(
            'flight' => __DIR__ . '/../view',
        ),
    ),
);
